import pandas as pd
import cv2
import numpy as np
import random

# 0を含めるため，整数値で-1して入力する．
IMG_COLS = 24
IMG_ROWS = 32
RED = 1
WHITE = 0

INPUT_PATH = 'input/'
TARGET = 'robocon.csv'
INIT = 'cer.csv'

OUTPUT_PATH = 'output/'


def frame2Img(frame):
    """
    2値のデータフレームを赤白画像へ変換して保存する。
    :param frame:
    :return:
    """
    plank = np.zeros((IMG_COLS, IMG_ROWS, 3), np.uint8)
    frame_m = frame.as_matrix()

    for j in range(IMG_ROWS):
        for i in range(IMG_COLS):
            if frame_m[i][j] == WHITE:
                plank[i][j]= (255, 255, 255)

            else:
                plank[i][j] = (0, 0, 255)

    # cv2.imshow('test', plank)
    # cv2.waitKey(0)

    return plank


def getDiffer(img1, img2):
    red_dif, white_dif = [], []
    red1, whi1 = 0, 0
    red2, whi2 = 0, 0

    for i in range(IMG_COLS):
        for j in range(IMG_ROWS):
            if (img1[i][j] == (0, 0, 255)).all():
                red1 += 1

            else:
                whi1 += 1

            if (img2[i][j] == (0, 0, 255)).all():
                red2 += 1

            else:
                whi2 += 1

            if not (img1[i][j] == img2[i][j]).all():
                if (img1[i][j] == (0, 0, 255)).all():
                    red_dif.append((i, j))

                else:
                    white_dif.append((i, j))

    if red1 != red2:
        print("points are wrong")

    return red_dif, white_dif


def changeRand(red_dif, white_dif, img):
    with open(OUTPUT_PATH + 'x.txt', 'wt') as x, open(OUTPUT_PATH + 'y.txt', 'wt') as y:
        for _ in range(len(red_dif)):
            num = random.randint(0, len(red_dif) - 1)

            i, j = img.shape[0], img.shape[1]

            if _%10 == 0:
                # cv2.imshow("img", img)
                # cv2.waitKey(0)
                cv2.imwrite(OUTPUT_PATH + '{}times.jpg'.format(_), cv2.resize(img, (i * 10, j * 10)))

            # pandasの仕様で一番上1行が消去される
            img[red_dif[num][0]][red_dif[num][1]] = (255, 255, 255)
            x.write(str(red_dif[num][1]) + '\n')
            y.write(str(red_dif[num][0]) + '\n')
            red_dif.pop(num)

            img[white_dif[num][0]][white_dif[num][1]] = (0, 0, 255)
            x.write(str(white_dif[num][1]) + '\n')
            y.write(str(white_dif[num][0]) + '\n')
            white_dif.pop(num)

    cv2.imwrite(OUTPUT_PATH + 'finally.jpg', cv2.resize(img, (i * 10, j * 10)))


if __name__ == '__main__':
    target = pd.read_csv(INPUT_PATH + TARGET)
    init_logo = pd.read_csv(INPUT_PATH + INIT)

    target = target.fillna(WHITE)
    init_logo = init_logo.fillna(WHITE)

    print(target.sum().sum())
    print(init_logo.sum().sum())
    target = frame2Img(target)
    init_logo = frame2Img(init_logo)

    red_dif, white_dif = getDiffer(init_logo, target)

    changeRand(red_dif, white_dif, init_logo)
